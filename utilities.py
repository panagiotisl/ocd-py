__author__ = 'panagiotis'
from itertools import combinations


def get_pair(edges_pairs):
    sa = {tuple(sorted(i)) for i in edges_pairs}
    return {tuple(sorted(t)) for t in combinations({i for j in edges_pairs for i in j}, 2) if tuple(sorted(t)) not in sa}