__author__ = 'Panagiotis Liakos'

"""A module for the calculation of dispersion"""

#import statistics as statistics
from lru import LRU

#common_neighbors = {}
common_neighbors = LRU(10000000)


#def swap(a, b):
#    if a > b:
#        return b, a
#    return a, b


def swap_to_string(a, b):
    if a > b:
        return b+':'+a
    return a+':'+b


def get_common_neighbors(graph, a, b):
    return list(set(graph[a]).intersection(graph[b]))


def get_or_find_common_neighbors(graph, a, b):
    pair = swap_to_string(a, b)
#    if pair in common_neighbors:
#        return common_neighbors[pair]
#    else:
#        cn = get_common_neighbors(graph, a, b)
#        common_neighbors[pair] = cn
#        return cn
    try:
        return common_neighbors[pair]
    except KeyError:
        cn = get_common_neighbors(graph, a, b)
        common_neighbors[pair] = cn
        return cn


def get_dispersion(graph, a, b):
    dispersion = 0
    cn = get_or_find_common_neighbors(graph, a, b)
    length = len(cn)
#       for all pairs of common neighbors calculate the distance (zero or one)
    for i in xrange(0, length - 1):
        s = cn[i]
        sn = graph[s]
        for j in xrange(i+1, length):
            t = cn[j]
            if t not in sn:
                st_cn = get_or_find_common_neighbors(graph, s, t)
                st_length = len(st_cn)
                if st_length <= 2:
                    dispersion += 1
    return max(dispersion, 1)


def get_normalized_dispersion_norm(graph, a, b):
    dispersion = 0
#       get common neighbors
    cn = get_common_neighbors(graph, a, b)
#       for all pairs of common neighbors calculate the distance (zero or one)
    for i in xrange(0, cn.len - 1):
        s = cn[i]
        sn = graph[s]
        for j in xrange(i+1, cn.len):
            t = cn[j]
            if t not in sn and get_common_neighbors(graph, s, t).len <= 2:
                dispersion += 1
    return float(dispersion)/cn.len


def get_normal_dispersion(graph, normalized=False):
    """Get all the edge dispersion measures. Input dict maps nodes to sets of neighbors.
    Output is a list of decorated edge-pairs, (1-sim,eij,eik), ordered by similarity.
    """
    print "computing dispersion..."
    dispersion_dict = {None: 0.5}
    count = 0
    for n in graph:  # n is the shared node
        for i in graph[n]:
            count += 1
            d = get_dispersion(graph, i, n)
            dispersion_dict[(swap_to_string(i, n))] = d
    return dispersion_dict  # return dict with dispersion of edge pairs


def get_or_find_dispersion(graph, dispersion, a, b):
    #pair = swap_to_string(a, b)
    pair = a+':'+b
    if pair in dispersion:
        return dispersion[pair]
    else:
        dispersion[pair] = get_dispersion(graph, a, b)
        return dispersion[pair]


def get_recursive_dispersion(graph):
    print "computing recursive dispersion..."
    #normal_dispersion = get_normal_dispersion(graph)
    max_disp = 1
    min_disp = 1
    normal_dispersion = {}
    dispersion_dict = {None: 0.5}
    temp_dict = {None: 0.5}
    for u in graph:  # n is the shared node
        for v in graph[u]:
            dispersion_dict[(swap_to_string(u, v))] = 1
    for iter in xrange(3):
        print 'Iteration', iter
        disp_pairs = set()
        for u in graph:  # n is the shared node
            for v in graph[u]:
                pair = swap_to_string(v, u)
                #
                if pair in disp_pairs:
                    continue
                disp_pairs.add(pair)
                c_uv = get_or_find_common_neighbors(graph, v, u)
                first = 0.0
                second = 0.0
                for w in c_uv:
                    first += dispersion_dict[(swap_to_string(u, w))]
                for s in xrange(len(c_uv)-1):
                    for t in xrange(s+1, len(c_uv)):
                        second += get_or_find_dispersion(graph, normal_dispersion, c_uv[s], c_uv[t]) * dispersion_dict[(swap_to_string(u, c_uv[s]))] * dispersion_dict[(swap_to_string(u, c_uv[t]))]
                value = float(first + 2 * second) / max(len(c_uv), 1)
                temp_dict[pair] = value
                if (iter == 2) and (value > max_disp):
                    max_disp = value
                if (iter == 2) and (value < min_disp):
                    min_disp = value
        #temp = dispersion_dict
        dispersion_dict = temp_dict
        temp_dict = {None: 0.5}
        #temp_dict = temp

    values = dispersion_dict.values()
    #print "MEAN_DISP ", statistics.mean(values)
    #print "STDEV_DISP ", statistics.stdev(values)
    #print "PSTDEV_DISP ", statistics.pstdev(values)
    print "MAX_DISP ", max(values)
    print "MIN_DISP ", min(values)
    return dispersion_dict, max_disp, min_disp # return dict with dispersion of edge pairs