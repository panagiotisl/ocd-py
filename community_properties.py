import link_clustering
from itertools import combinations


def probability():
    adj, edges = link_clustering.read_edgelist_unweighted('lesmiserables/lesmiserables.txt', '\t')

    total = {}
    true = {}
    for i, j in combinations(adj, 2):
        count = 0
        edge_exists = i in adj[j] or j in adj[i]
        with open('mis_nd') as f:
            for line in f:
                comm = line.split()
                if i in comm and j in comm:
                    count += 1
        if count in total:
            total[count] += 1
        else:
            total[count] = 1
        if edge_exists:
            if count in true:
                true[count] += 1
            else:
                true[count] = 1

        #if count > 0:
        #    print i, j, edge_exists, count
    print total
    print true


def comm_overlap_distr(communities):
    print 'comm_overlap_distr', communities
    comms = []
    distr = {}
    with open(communities) as f:
        for line in f:
            comm = line.split()
            comms.append(comm)
    for i, j in combinations(comms, 2):
        length = len(set(i).intersection(j))
        if length in distr:
            distr[length] += 1
        else:
            distr[length] = 1
    print distr


comm_overlap_distr('dblp_nd')