#!/usr/bin/env python

# set up, load packages, etc

import re

# read philosophers
ph = open('philosophers.txt','r')
philosophers = set([])
for line in ph:
    str = line.strip().split('title=\"')
    str = str[1]
    name = str.split("\"")
    name = name[0].lower()
    if name[-21:] == '(page does not exist)':
        pass
    else:
        philosophers.add(name.replace(',',';')) # since the commas cause problems later on
        
print 'Number of philosophers: %s' % len(philosophers)

# files
f = open('philosophy_wiki.xml','r')
fout = open('philosophy_edgelist.txt','w')
fout1 = open('philosophy_categories.txt', 'w')
fout2 = open('philosophy_outlinks.txt', 'w')

# read articles
page = []
pagecount = 0

for i,line in enumerate(f):
    lvar = line.strip()
    page.append(lvar)

    #if pagecount > 0: break

    if lvar == '<page>':
        page = []
        page.append(lvar)
    elif lvar == '</page>':
        pagecount += 1
        page.append(lvar)
        
        # title is always on line 2
        title = page[1][7:][:-8].lower().replace(',',';')
        
        # extract all external wiki links
        pagelinks = []
        for ll in page:
            # get square brackets
            temp_links = re.findall("\[\[.*?\]\]",ll)  # note that this approach misses a few links (inside image links)
            for sl in temp_links:
                if len(sl) > 0:
                    pagelinks.append( sl[2:-2] )

        # sort the wiki links into categories
#       There are various kinds of interesting link types
#       * [[Wiki link]], [[Wiki link|link]]
#       * [[Category:Some Category]]
#       * [[Image:Some image link]]

        categories = []
        wikilinks = []
        for ll in pagelinks:
            if ll[:9] == 'Category:':
                if ll[-2:] == '|*': # some subtleties regarding the formatting of categories
                    pass
                elif ll[-2:] == "| ":
                    pass
                else:
                    categories.append(ll[9:].lower().replace(',',';'))
            elif ll[:6] == 'Image:':
                pass
            elif re.search(":",ll):
                pass    # don't use the weird extra links
            else:
                sll = ll.split('|') # take into account the formatting links
                wikilinks.append( sll[0].lower().replace(',',';') )


        # write edgelist
        for ll in wikilinks:
            if (ll in philosophers) and (ll != title):
                fout.write('%s %s\n' % ( ll.replace(' ','_'), title.replace(' ','_') ) )
        
        # write categories
        fout1.write('%s' % title.replace(' ','_'))
        for cc in categories:
            fout1.write(', %s' % cc)
        fout1.write('\n')


        # write outlinks
        fout2.write('%s' % title.replace(' ','_'))
        for ll in wikilinks:
            if ll not in philosophers:
                fout2.write(", %s" % ll)
        fout2.write('\n')


fout.close()
fout1.close()
fout2.close()
