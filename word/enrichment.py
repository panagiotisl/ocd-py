#!/usr/bin/env python
# encoding: utf-8
import sys
import os
import time
from collections import defaultdict
from itertools import combinations
from numpy import mean
import glob

def load_meanings():
    meanings = {}
    for i in open('meanings.txt'):
        temp = i.strip().split()
        word = temp.pop(0)
        meanings[word] = set(temp)
    return meanings

def load_all_words():
    all_words = set()
    for i in open('word.net'):
        a,b = i.strip().split('\t')
        all_words.add(a)
        all_words.add(b)
    return all_words

def is_similar(i,j, meanings):
    if meanings[i] & meanings[j]:
        return 1 
    else:
        return 0

def enrichment(aset, meanings):
    sim_list = []
    for i, j in combinations(aset, 2):
        if i in meanings and j in meanings:
            sim_list.append(is_similar(i,j, meanings))
    return sim_list

if __name__=='__main__':
    meanings = load_meanings()
    
    try:
        cmemf = sys.argv[1] 
    except:
        cmemf = 'word_lc.cmem'
    result = []
    for i in open(cmemf):
        temp = [x.lower().replace(' ', '_') for x in i.strip().split('\t')[1:]]
        result += enrichment(temp, meanings)
    m_comm = mean(result)
    
    fname = 'all_similarity_value.txt'
    if glob.glob(fname):
        m_null = float(open(fname).read().strip())
    else:
        all_words = set(x.lower().replace(' ', '_') for x in load_all_words())
        alist = enrichment(all_words, meanings)
        m_null = mean(alist)
        print >>open(fname, 'w'), m_null
    print m_comm/m_null
        



