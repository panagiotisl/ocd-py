__author__ = 'panagiotis'

import unittest
import link_clustering
import dispersion
import measures
import os
import statistics


class DemonTestCase(unittest.TestCase):
    def test_congress(self):
        demon_clusters_filename = 'congress/demon_communities'
        demon_clusters = measures.get_non_trivial_clusters(measures.get_clusters_from_file_word(demon_clusters_filename))
        ground_truth_clusters_filename = 'congress/congress_person_subject_remap'
        ground_truth_clusters = measures.get_non_trivial_clusters(measures.get_clusters_from_file_demon(ground_truth_clusters_filename))
        f1_score, f2_score = measures.get_average_f_score(demon_clusters, ground_truth_clusters)
        print f1_score, f2_score, '(', len(demon_clusters), ',', len(ground_truth_clusters), ')'