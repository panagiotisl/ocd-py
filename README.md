# About #

Code for finding overlapping communities in complex networks. The code is built using the implementation of [1] found in [2].

### References ###

1. Yong-Yeol Ahn, James P. Bagrow, and Sune Lehmann, Link communities reveal multiscale complexity in networks, Nature 466, 761 (2010).

2. https://github.com/bagrow/linkcomm

### Who do I talk to? ###

* Panagiotis Liakos, http://cgi.di.uoa.gr/~grad0990