__author__ = 'panagiotis'


"""A module for the calculation of different measures"""


from itertools import combinations
import math
import statistics


def get_clusters_from_file(filename, delimiter=', '):
    clusters = {}
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            string = line.strip().split(delimiter)
            for cluster in string[1:]:
                if cluster in clusters:
                    clusters[cluster].add(string[0])
                else:
                    clusters[cluster] = set()
                    clusters[cluster].add(string[0])
    return list(clusters.values())


def get_clusters_from_file_snap(filename):
    clusters = []
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            line_set = set()
            string = line.strip().split('\t')
            for category in string:
                line_set.add(category)
            clusters.append(line_set)
    return clusters


def get_clusters_from_file_snap2(filename):
    clusters = []
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            line_set = set()
            string = line.split('\t')
            for category in string[1:]:
                line_set.add(category)
            clusters.append(line_set)
    return clusters


def get_clusters_from_file_bigclam(filename):
    clusters = []
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            line_set = set()
            string = line.split('\t')
            for category in string:
                line_set.add(category)
            clusters.append(line_set)
    return clusters


def get_clusters_from_file_demon(filename):
    clusters = {}
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            string = line.split('\t')
            cluster = string[1]
            if cluster in clusters:
                clusters[cluster].add(string[0])
            else:
                clusters[cluster] = set()
                clusters[cluster].add(string[0])
    return list(clusters.values())


def get_non_trivial_clusters(clusters):
    non_trivial_clusters = []
    for cluster in clusters:
        if len(cluster) > 2:
            non_trivial_clusters += [cluster]
    return non_trivial_clusters


def get_top_k_largest_clusters(clusters, k=10):
    clusters.sort(lambda x, y: -cmp(len(x), len(y)))
    return clusters[:k]


def get_best_cluster(cluster, ground_truth_clusters):
    best_cluster = set()
    max_size = 0
    for ground_truth_cluster in ground_truth_clusters:
        common_elements = set.intersection(cluster, ground_truth_cluster)
        if len(common_elements) > max_size:
            max_size = len(common_elements)
            best_cluster = ground_truth_cluster
    return best_cluster


def get_argmaxf_cluster(cluster, ground_truth_clusters):
    best_f1 = 0.0
    best_f2 = 0.0
    for ground_truth_cluster in ground_truth_clusters:
        f1, f2 = get_f_score(cluster, ground_truth_cluster)
        if f1 > best_f1:
            best_f1 = f1
        if f2 > best_f2:
            best_f2 = f2
    return best_f1, best_f2


def get_precision(cluster1, cluster2):
    common_elements = cluster1.intersection(cluster2)
    return float(len(common_elements))/len(cluster1)


def get_recall(cluster1, cluster2):
    common_elements = cluster1.intersection(cluster2)
    return float(len(common_elements))/len(cluster2)


def get_f_score(cluster1, cluster2):
    precision = get_precision(cluster1, cluster2)
    recall = get_recall(cluster1, cluster2)
    if precision == 0 and recall == 0:
        return 0.0, 0.0
    return 2 * (precision * recall) / (precision + recall), 5 * (precision * recall) / (4*precision + recall)


def get_average_f_score(clusters, ground_truth_clusters):
    print "Calculating average f1 score"
    first_sum = 0.0
    first_count = 0
    first_sumf2 = 0.0
    for ground_truth_cluster in ground_truth_clusters:
        #best_cluster = get_best_cluster(ground_truth_cluster, clusters)
        #if len(best_cluster) == 0:
        #    first_count += 1
        #    continue
        #first_sum += get_f1_score(ground_truth_cluster, best_cluster)
        f1, f2 = get_argmaxf_cluster(ground_truth_cluster, clusters)
        first_sum += f1
        first_count += 1
        first_sumf2 += f2
    second_sum = 0.0
    second_count = 0
    second_sumf2 = 0.0
    for cluster in clusters:
        #best_cluster = get_best_cluster(cluster, ground_truth_clusters)
        #if len(best_cluster) == 0:
        #    second_count += 1
        #    continue
        #second_sum += get_f1_score(cluster, best_cluster)
        f1, f2 = get_argmaxf_cluster(cluster, ground_truth_clusters)
        second_sum += f1
        second_count += 1
        second_sumf2 += f2
    return 0.5 * ((first_sum / first_count) + (second_sum / second_count)), 0.5 * ((first_sumf2 / first_count) + (second_sumf2 / second_count))


def get_omega_index(graph, clusters, ground_truth_clusters):
    count = 0
    total_sum = 0
    keys = graph.keys()
    for u, v in combinations(keys, 2):
        #print u, v
        for c in clusters:
            if u in c and v in c:
                print u, v, c
                count += 1
        for gtc in ground_truth_clusters:
            if u in gtc and v in gtc:
                count -= 1
        if count == 0:
            total_sum += 1
    print total_sum, math.pow(len(keys), 2)
    return total_sum / math.pow(len(keys), 2)


def get_community_coverage(graph, clusters):
    total = 0
    covered = 0
    nodes = graph.keys()
    for node in nodes:
        total += 1
        for cluster in clusters:
            if node in cluster:
                covered += 1
                break
    print covered, total, covered / float(total)
    return covered / float(total)


def get_overlap_coverage(graph, clusters):
    coverage = []
    cluster_size = []
    nodes = graph.keys()
    for node in nodes:
        count = 0
        for cluster in clusters:
            cluster_size.append(len(cluster))
            if node in cluster:
                count += 1
        coverage.append(count)
#    print statistics.mean(cluster_size)
    return statistics.mean(coverage)
#    return 1


def get_accuracy_in_the_number_of_communities(clusters, ground_truth_clusters):
    return 1 - (abs(len(ground_truth_clusters)-len(clusters))/2*len(ground_truth_clusters))


def find_conductance(graph, edges, clusters):

    conductance = 0
    total_clusters = 0

    total_edges = len(edges)

    #print "TOTAL EDGES:"
    #print total_edges

    for cluster in clusters:
        internal = 0
        external = 0
        for node in cluster:
            for neighbor in graph[node]:
                if neighbor > node:
                    if neighbor in cluster:
                        internal += 1
                    else:
                        external += 1
        if len(cluster) > 0:
            #print len(cluster), float(external)/internal, external, internal
            conductance += float(external)/internal
            total_clusters += 1
    #print conductance / total_clusters

    #extract the nodes of interest from graph
    #node_list = graph[name]
    #nodes = re.split(', ', node_list)
    #nodes = [int(entry) for entry in nodes]

    #get vertex set and subgraph set to compare differences
    #vertices = graph.vs.select(nodes)
    #subgraph = graph.subgraph(nodes)

    #get relevant data from vertex set and subgraph
    #edges_inside = float(sum(subgraph.degree()))
    #total_edges = float(sum(vertices.degree()))
    return conductance / total_clusters
    #return str((total_edges - edges_inside) / total_edges)
