__author__ = 'panagiotis'

import unittest
import link_clustering
import measures
import os
import dispersion


class WordTestCase(unittest.TestCase):
    def test_with_dispersion(self):
        delimiter = '\t'
        threshold = None
        is_weighted = False
        dendro_flag = False

        edgelist_filename = 'word/word.net'
        ground_truth_clusters_filename = 'word/word_lc.cmem'
        print "# loading network from edgelist..."
        basename = os.path.splitext(edgelist_filename)[0]
        if is_weighted:
            adj, edges, ij2wij = link_clustering.read_edgelist_weighted(edgelist_filename, delimiter=delimiter)
        else:
            adj, edges = link_clustering.read_edgelist_unweighted(edgelist_filename, delimiter=delimiter)

        Disp, max_disp, min_disp = dispersion.get_recursive_dispersion(adj)
        print "max-min dispersion: ", max_disp, min_disp

        for high in xrange(1000, 20001, 2000):
            disp_scaled = link_clustering.get_scaled_dispersion(Disp, max_disp, min_disp, high)
            print "# measuring average f1 score using dispersion", high
            # run the method:
            if threshold is not None:
                if is_weighted:
                    edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, w=ij2wij)
                else:
                    edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, Disp=disp_scaled)
                print "# D_thr = %f" % D_thr
                clusters = link_clustering.write_edge2cid(edge2cid, "%s_thrS%f_thrD%f" % (basename, threshold, D_thr), delimiter=delimiter)
                clusters = measures.get_non_trivial_clusters(clusters)
                ground_truth_clusters = measures.get_non_trivial_clusters(measures.get_clusters_from_file_snap2(ground_truth_clusters_filename))

                f1_score = measures.get_average_f1_score(clusters, ground_truth_clusters)
                print f1_score, '(', len(clusters), ',', len(ground_truth_clusters), ')'

                cond = measures.find_conductance(adj, edges, clusters)
                print str(cond), str(len(clusters))

            else:
                if is_weighted:
                    edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(w=ij2wij)
                else:
                    if dendro_flag:
                        edge2cid, S_max, D_max, list_D, orig_cid2edge, linkage = link_clustering.HLC(adj, edges).single_linkage(
                            dendro_flag=dendro_flag)
                        link_clustering.write_dendro("%s_dendro" % basename, orig_cid2edge, linkage)
                    else:
                        edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(high=high, Disp=disp_scaled)

                f = open("%s_thr_D.txt" % basename, 'w')
                for s, D in list_D:
                    print >> f, s, D
                f.close()
                print "# D_max = %f\n# S_max = %f" % (D_max, S_max)
                clusters = link_clustering.write_edge2cid(edge2cid, "%s_maxS%f_maxD%f" % (basename, S_max, D_max), delimiter=delimiter)
                clusters = measures.get_non_trivial_clusters(clusters)
                ground_truth_clusters = measures.get_non_trivial_clusters(measures.get_clusters_from_file_snap2(ground_truth_clusters_filename))

                f1_score = measures.get_average_f1_score(clusters, ground_truth_clusters)
                print f1_score, '(', len(clusters), ',', len(ground_truth_clusters), ')'

                cond = measures.find_conductance(adj, edges, clusters)
                print str(cond), str(len(clusters)), str(D_max)
                #self.assertEqual(0.17976190101529377, f1_score)

    def test_without_dispersion(self):
        delimiter = '\t'
        threshold = None
        is_weighted = False
        dendro_flag = False

        edgelist_filename = 'word/word.net'
        ground_truth_clusters_filename = 'word/word_lc.cmem'

        print "# measuring average f1 score without dispersion"
        print "# loading network from edgelist..."
        basename = os.path.splitext(edgelist_filename)[0]
        if is_weighted:
            adj, edges, ij2wij = link_clustering.read_edgelist_weighted(edgelist_filename, delimiter=delimiter)
        else:
            adj, edges = link_clustering.read_edgelist_unweighted(edgelist_filename, delimiter=delimiter)
        # run the method:
        if threshold is not None:
            if is_weighted:
                edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, w=ij2wij)
            else:
                edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, with_dispersion=False)
            print "# D_thr = %f" % D_thr
            clusters = link_clustering.write_edge2cid(edge2cid, "%s_thrS%f_thrD%f" % (basename, threshold, D_thr), delimiter=delimiter)
            clusters = measures.get_non_trivial_clusters(clusters)
            ground_truth_clusters = measures.get_non_trivial_clusters(measures.get_clusters_from_file_snap2(ground_truth_clusters_filename))

            f1_score = measures.get_average_f1_score(clusters, ground_truth_clusters)
            print f1_score, '(', len(clusters), ',', len(ground_truth_clusters), ')'

            cond = measures.find_conductance(adj, edges, clusters)
            print str(cond), str(len(clusters))
        else:
            if is_weighted:
                edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(w=ij2wij)
            else:
                if dendro_flag:
                    edge2cid, S_max, D_max, list_D, orig_cid2edge, linkage = link_clustering.HLC(adj, edges).single_linkage(
                        dendro_flag=dendro_flag, with_dispersion=False)
                    link_clustering.write_dendro("%s_dendro" % basename, orig_cid2edge, linkage)
                else:
                    edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(with_dispersion=False)

            f = open("%s_thr_D.txt" % basename, 'w')
            for s, D in list_D:
                print >> f, s, D
            f.close()
            print "# D_max = %f\n# S_max = %f" % (D_max, S_max)
            clusters = link_clustering.write_edge2cid(edge2cid, "%s_maxS%f_maxD%f" % (basename, S_max, D_max), delimiter=delimiter)
            clusters = measures.get_non_trivial_clusters(clusters)
            ground_truth_clusters = measures.get_non_trivial_clusters(measures.get_clusters_from_file_snap2(ground_truth_clusters_filename))

            f1_score = measures.get_average_f1_score(clusters, ground_truth_clusters)
            print f1_score, '(', len(clusters), ',', len(ground_truth_clusters), ')'

            cond = measures.find_conductance(adj, edges, clusters)
            print str(cond), str(len(clusters)), str(D_max)
            #self.assertEqual(0.14491639512409635, f1_score)


if __name__ == '__main__':
    unittest.main()
